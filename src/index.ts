import yargs from 'yargs';
import {hideBin} from 'yargs/helpers';
import {glob} from "glob";
import {promisify} from "util";
import * as fs from "fs";
import * as natural from 'natural';

/**
 * Available formats
 */
const format = ['json' as const, 'text' as const];

/**
 * Format but as type
 */
type Format = (typeof format)[0];

/**
 * Interface defining app arguments
 */
interface Argv {
    inputFiles: string
    outputFile: string | undefined
    format: Format
    clusterScore: number
}

/**
 * Interface of a sample file (path + content)
 */
interface Sample {
    path: string,
    content: string
}

/**
 * Interface of a sample during processing
 */
type SampleProcessing = {
    tokens: string[]
} & Sample;

/**
 * Main program loop
 */
async function run() {
    const argv: Argv = await yargs(hideBin(process.argv))
        .scriptName("Plagiarism")
        .option('inputFiles', {
            type: 'string',
            demandOption: true,
            describe: 'Glob expression to files containing answers. Each file\n' +
                'contains a single text to compare to each others. It assumes\n' +
                'all texts refer to the same question.'
        })
        .option('outputFile', {
            type: 'string',
            describe: 'Path to the output file. If missing, output to console.',
        })
        .option('format', {
            choices: format,
            default: 'text' as Format,
            describe: 'The output format.',
        })
        .option('clusterScore', {
            type: 'number',
            default: 0.9,
            describe: 'The minimum matching score to merge 2 submissions in the same\n' +
                'report. It is a number between 0 (merge even if not really\n' +
                'similar) and 1 (merge only if exactly same). Default is 0.9.'
        })
        .parse();

    // Processing
    const samples = await readFiles(await listFiles(argv.inputFiles));
    const clusters = computePlagiarismClusters(samples, {clusterScore: argv.clusterScore});


    // Output handling
    const clustersInString = generateOutputString(clusters);
    console.log(clustersInString);

    if(argv.outputFile as string) {
        await writeOutputFile(argv.outputFile as string, clustersInString, argv.format, clusters);
    }
}

/**
 * List files based on string glob
 * @param inputFiles String glob
 */
async function listFiles(inputFiles: string): Promise<string[]> {
    const globPromise = promisify(glob);
    return await globPromise(inputFiles);
}

/**
 * Find if a sample is in clusters
 * @param clusters Clusters
 * @param sample Sample to check
 * @return boolean True if in any cluster
 */
function findSampleInCluster(clusters: Sample[][], sample: Sample): boolean {
    return clusters.some(cluster => cluster.find(s => s.path === sample.path))
}

/**
 * Read files and their content
 * @param files List of files to read
 */
async function readFiles(files: string[]): Promise<Sample[]> {
    const readFile = promisify(fs.readFile);
    // We need to wait for all promise
    return await Promise.all(files.map(async (elt) => {
        return {path: elt, content: await readFile(elt, 'utf-8')}
    }));
}

/**
 * Generate the representation of the cluster as a JSON
 * @param clusters Clusters
 * @return String of JSON
 */
function generateClusterJSON(clusters: Sample[][]): string {
    // Formatting data (we only keep the path)
    const clustersPath: string[][] = [];
    clusters.forEach(elt => {
        clustersPath.push(elt.map(e => e.path));
    });

    // Converting obj to string
    return JSON.stringify({
        numberClusters: clusters.length,
        clusters: clustersPath
    });
}

/**
 * Write the output file
 * @param fileName Name of file (without extension)
 * @param clustersInString Representation of clusters, but in string
 * @param format Format of file
 * @param clusters Clusters
 */
async function writeOutputFile(fileName: string, clustersInString: string, format: Format, clusters: Sample[][]) {
    const writeFile = promisify(fs.writeFile);
    let data = clustersInString;
    let extension = "txt";

    if (format === 'json' as Format) {
        data = generateClusterJSON(clusters);
        extension = 'json';
    }
    await writeFile(`${fileName}.${extension}`, data);
}

/**
 * Compute compatibility score between two samples
 * @param p First sample in process
 * @param s Second sample in process
 * @return score Compatibility score
 */
function scoreComputation(p: SampleProcessing, s: SampleProcessing) {
    // Distance is computed from tokens joined together
    const distance = natural.DamerauLevenshteinDistance(p.tokens.join(''), s.tokens.join(''));
    // Score is 1 - distance / length
    return 1 - distance / Math.max(p.tokens.join('').length, s.tokens.join('').length);
}

/**
 * Format samples into samples processing (generate tokens and stem them)
 * @param samples Samples processing
 * @return SampleProcessing[] Samples ready for processing
 */
function dataFormatting(samples: Sample[]): SampleProcessing[] {
    const tokenizer = new natural.WordTokenizer();

    // Tokenizing each word of each text
    const processing: SampleProcessing[] = samples.map(e => {
        return {content: e.content, path: e.path, tokens: tokenizer.tokenize(e.content)}
    })

    // Stemming each token
    processing.forEach(p => p.tokens.map(
        token => natural.PorterStemmer.stem(token))
    );
    return processing;
}

/**
 * Print and update progress (single line with updates)
 * @param progress File being process
 */
function printProgress(progress: string){
    process.stdout.clearLine(-1);
    process.stdout.cursorTo(0);
    process.stdout.write(progress);
}

/**
 * Compare a sample with a cluster.
 * If sample is compatible with cluster (each element of the cluster), it is inserted inside
 * @param processedSample Sample to compare cluster with
 * @param cluster Cluster
 * @param minScore Minimal score for sample to be added inside cluster
 * @return boolean True if sample was inserted
 */
function compareSampleWithCluster(processedSample: SampleProcessing, cluster: SampleProcessing[], minScore: number) {
    // First we check if every sample inside a cluster pass the min score with the processed sample
    let validCluster = true;
    for (const s of cluster) {
        const score = scoreComputation(processedSample, s);
        if (score < minScore) {
            validCluster = false;
            break;
        }
    }
    // If each element of the cluster has a score high enough, it can be added inside the cluster
    if (validCluster) {
        cluster.push(processedSample);
        return true;
    }
    return false;
}

/**
 * Create clusters from SampleProcessing
 * @param processing Samples
 * @param options Min cluster score
 * @return SampleProcessing[][] Clusters
 */
function createClusters(processing: SampleProcessing[], options: { clusterScore: number }): SampleProcessing[][] {
    const clusters: SampleProcessing[][] = [];

    // We compare each sample to each cluster
    for (const p of processing) {
        printProgress("Currently processing " + p.path);
        for (const c of clusters) {
            if(compareSampleWithCluster(p,c,options.clusterScore)) break;
        }
        if (!findSampleInCluster(clusters, p)) {
            clusters.push([p]);
        }
    }
    printProgress("");

    // Removing cluster with only element
    return clusters.filter(c => c.length > 1);
}

/**
 * Groups samples into cluster depending on how similar they are.
 * If a sample not far from all other samples, it is not returned.
 * A cluster is basically an array of samples, and this function returns a array of clusters.
 * Each sample has both an identifier (ex. the path to the original file) and a content.
 * @param samples List of samples to analyse
 * @param options Options used during analysis
 * @return Sample[][] Clusters containing samples
 */
function computePlagiarismClusters(samples: Sample[], options: { clusterScore: number }
): Sample[][] {
    const processing = dataFormatting(samples);
    return createClusters(processing, options);
}

/**
 * Generate the representation of the cluster as a text
 * @param clusters Clusters
 * @return String text
 */
function generateOutputString(clusters: Sample[][]): string {
    // Length of cluster
    const stringBuilder: string[] = [];
    const clusterSize = clusters.length;
    stringBuilder.push(`Number of clusters: ${clusters.length}`);

    // If we have clusters, we only extract the path
    if(clusterSize > 0) {
        stringBuilder.push('\n');
        clusters.forEach(elt => {
            stringBuilder.push("- ");
            elt.forEach(c => stringBuilder.push(c.path + " "));
            stringBuilder.push('\n');
        });
    }

    return stringBuilder.join('');
}


run().catch(console.error);