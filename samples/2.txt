Pull request and code review

Making pull request allows the other members of the team to review the code written in a feature branch before merging it into develop (an example). It allows to discover errors that were not seen by the developer and the team members can make suggestion to improve it. It allows communication inside a team and a good cohesion between tema members. 