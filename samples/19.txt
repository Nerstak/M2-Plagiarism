The advantage of a linter like Eslint:

-ERROR AND WARNING INDICATIONS during code writing without a need to run said code. It can also detect potential issues and propose cleaner/more robust implementation. This allow for easier and faster debug.

-CODE STYLE ADHERENCe check. This allow a unified coding style and therefore a cleaner and easier to read code.