PULL REQUEST AND CODE REVIEW

Those are two essentials part of version control platforms as GitHub. After developing a new feature on GitHub, a developer might want to merge its branch into an other one to share it with the rest of the team. However, it can create some problems such as conflicts. To avoid it, the developer can make a pull request : He asks to the other developers to do a code review. It means they have to check the code, do some commentaries if they want and eventually correct mistakes or improve some parts.

This way, the team work together and improve communication skills. Nevertheless, everybody in the team will be able to propose its vision of the code, and they will choose together the best version.

Pull request and code review help developers to see solutions to their problems or see new way of coding. It improves the code's quality, developers skills and team skills.